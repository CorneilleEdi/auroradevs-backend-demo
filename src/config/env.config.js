const config = {
    name: 'User Management Service',
    port: process.env.PORT || 5000,
    environment: process.env.ENVIRONMENT || 'dev',
    db: {
      uri: process.env.DB_URI || 'mongodb://localhost:27017/users_api',
      username: process.env.DB_USERNAME || '',
      password: process.env.DB_PASSWORD || '',
    },
  };
  
  module.exports = config;
  