const http = require("http");
const app = require("./app");
const config = require('./config/env.config');
const PORT = config.port;

// Init the environment variables and server configurations
require('dotenv').config();


app.listen(PORT, () => {
    console.log(`Server up and running on port ${PORT}`);

})