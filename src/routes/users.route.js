const express = require("express");
const router = express.Router();
const users = require("../controllers/users.controller");

/**
 * Add a get request handler on the route
 * Get request at the / endpoint
 */
router.get("/", users.getUsers);
router.get("/:uid", users.getUser);
router.post("/", users.createUser);
router.patch("/:uid", users.updateUser);
router.delete("/:uid", users.deleteUser);
router.delete("/", users.deleteAllUser);

module.exports = router;