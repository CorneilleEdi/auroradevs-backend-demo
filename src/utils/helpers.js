const helpers = {
    /**
     * Check if the lenght of the str is greater than l
     * @param {String} str
     * @param {Number} l
     * @returns {Boolean}
     */
    isValidLength: (str, l) => {
        return str.length > l;
    },

    /**
     * Check if the email is a valid one (example@mail.com)
     * @param {String} email
     * @returns {Boolean}
     */
    isValidEmail: (email) => {
        const valid = new RegExp(/^[^@\s]+@[^@\s]+\.[^@\s]+$/)
        return valid.test(email)
    }
}


module.exports = helpers