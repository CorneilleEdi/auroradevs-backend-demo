const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    uid: { type: String, require: true, unique: true },
    name: { type: String, require: true },
    email: { type: String, require: true, unique: true },
    about: { type: String, default:'' },
    createdDate: { type: Date, default: Date.now },
}, { versionKey: false });

module.exports = mongoose.model('User', UserSchema);
