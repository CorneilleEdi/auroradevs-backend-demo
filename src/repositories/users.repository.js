const UserModel = require('../models/users.model');
class UsersRepository {

    /**
     * Get all users information
     * @returns {Promise} Promise<Users>
     */
    getUsers() {
        return UserModel.find({});
    }

    /**
     * Get one user information
     * @param {String} uid
     * @returns {Promise} Promise<Users>
     */
    getUser(uid) {
        return UserModel.findOne({ uid });
    }

    /**
    * Create new user
    * @param {Object} info
    * @returns {Promise} Promise<Users>
    */
    createUser(info) {
        const user = UserModel(info);
        return user.save();
    }

    /**
   * Update user information
   * @param {String} uid
   * @param {Object} info
   * @returns {Promise} Promise<Users>
   */
    updateUser(uid, info) {
        return UserModel.findOneAndUpdate({ uid }, info)
    }

    /**
    * Delete user
    * @param {String} uid
    * @returns {Promise} Promise<Users>
    */
    deleteUser(uid) {
        return UserModel.findOneAndRemove({ uid })
    }

    /**
    * Delete all user
    * @param {String} uid
    * @returns {Promise} Promise<Users>
    */
   deleteAllUsers() {
    return UserModel.deleteMany({})
}
}

module.exports = UsersRepository;