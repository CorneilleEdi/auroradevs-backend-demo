class NotFoundException extends Error {
    constructor(status, value) {


        super(`${value} not found.`)
        this.status = status

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, NotFoundException)
        }
    }
}


module.exports = NotFoundException