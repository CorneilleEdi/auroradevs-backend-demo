class RequiredParameterException extends Error {
    constructor(status, value) {
        super(`${value} is required and well formatted.`)

        this.status = status

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, RequiredParameterException)
        }
    }
}


module.exports = RequiredParameterException