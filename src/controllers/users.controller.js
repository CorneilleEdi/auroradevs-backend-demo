const UsersService = require('../services/users.service');

const usersService = new UsersService();

/**
 * Get users information and send response
 */
exports.getUsers = async (request, response) => {

    try {
        const users = await usersService.getUsers()
        return response.status(200).json(users);
    } catch (error) {
        return response.status(500).send({
            message: error.message || "Some error occurred while getting the users info."
        });
    }
}

/**
 * Get users information and send response
 */
exports.getUser = async (request, response) => {
    const uid = request.params.uid;

    try {
        const users = await usersService.getUser(uid)
        return response.status(200).json(users);
    } catch (error) {
        return response.status(error.status || 500).send({
            message: error.message || "Some error occurred while getting the user info."
        });
    }
}

/**
 * Create new user
 */
exports.createUser = async (request, response) => {

    try {
        const info = request.body;
        const users = await usersService.createUser(info)
        return response.status(201).json(users);
    } catch (error) {
        return response.status(error.status || 500).send({
            message: error.message || "Some error occurred while creating the user info."
        });
    }
}

/**
 * Update new user
 */
exports.updateUser = async (request, response) => {

    try {
        const uid = request.params.uid;
        const info = request.body;
        const users = await usersService.updateUser(uid, info)
        return response.status(202).json(users);
    } catch (error) {
        return response.status(error.status || 500).send({
            message: error.message || "Some error occurred while updating the user info."
        });
    }
}


/**
 * Update new user
 */
exports.deleteUser = async (request, response) => {

    try {
        const uid = request.params.uid;
        const users = await usersService.deleteUser(uid)
        return response.status(200).json(users);
    } catch (error) {
        return response.status(error.status || 500).send({
            message: error.message || "Some error occurred while deleting the user info."
        });
    }
}



/**
 * Update new user
 */
exports.deleteAllUser = async (request, response) => {

    try {
        const r = await usersService.deleteAllUser()
        return response.status(200).json(r);
    } catch (error) {
        return response.status(error.status || 500).send({
            message: error.message || "Some error occurred while deleting the user info."
        });
    }
}