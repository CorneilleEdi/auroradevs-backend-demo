const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const helmet = require("helmet");

const config = require("./config/env.config");
const userRoutes = require("./routes/users.route");

// Connection to the database
const connectionOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
  user: config.db.username,
  pass: config.db.password
};
mongoose
  .connect(config.db.uri, connectionOptions)
  .then(() => {
    console.log("Connected to db");
  })
  .catch(e => {
      console.log(`DB connection error : ${e.message}`);
      
  });
mongoose.Promise = global.Promise;

// Setting up the server
const app = express();
app.use(helmet());
app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// setting up the base endpoint for users request handling
app.use("/api/users", userRoutes);

module.exports = app;
