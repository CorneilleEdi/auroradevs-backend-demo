const UsersRepository = require('../repositories/users.repository');
const helper = require('../utils/helpers')
const RequiredParameterException = require('../exceptions/required_param.exception')
const NotFoundException = require('../exceptions/not_found.exception')
const uuid4 = require('uuid/v4');
class UsersService {

    // This methods are the one that will be modified to add more logic

    usersRepo = new UsersRepository();

    /**
     * Get all users information
     * @returns {Promise} Promise<Users>
     */
    async getUsers() {
        try {
            return await this.usersRepo.getUsers();
        } catch (error) {
            throw error
        }
    }

    /**
     * Get one user information
     * @param {String} uid
     * @returns {Promise} Promise<User>
     */
    async getUser(uid) {
        try {
            if (!uid || !helper.isValidLength(uid, 8)) {
                throw new RequiredParameterException(400, 'uid')
            }
            const result = await this.usersRepo.getUser(uid);

            if (!result) {
                throw new NotFoundException(404, 'User')
            }

            return result;
        } catch (error) {
            throw error
        }

    }

    /**
     * Create new user
     * @param {Object} info
     * @returns {Promise} Promise<Users>
     */

    async createUser(info) {
        try {

            const { name, email } = info;

            if (!name || !helper.isValidLength(name, 4) || !email) {
                throw new RequiredParameterException(400, 'name or email')
            }

            if (!helper.isValidEmail(email)) {
                throw new RequiredParameterException(400, 'email')
            }

            info.uid = uuid4();
            return await this.usersRepo.createUser(info)
        } catch (error) {
            // Email must be unique. Catch error thrwon when duplicated email
            // 11000 error code for mongoose unique filed eroor
            if (error.code == '11000') {
                throw Error("Failed, please try with new information")
            }
            throw error
        }
    }

    /**
  * Update user information
  * @name UpdateUser
  * @param {String} uid
  * @param {Object} info
  * @returns {Promise} Promise<Users>
  */

    async updateUser(uid, info) {
        try {

            const { name, email } = info;

            // Check uid
            if (!uid || !helper.isValidLength(uid, 8)) {
                throw new RequiredParameterException(400, 'uid')
            }

            // Name and email validation
            if (!name || !helper.isValidLength(name, 4) || !email) {
                throw new RequiredParameterException(400, 'name or email')
            }

            if (!helper.isValidEmail(email)) {
                throw new RequiredParameterException(400, 'email')
            }

            return await this.usersRepo.updateUser(uid, info)
        } catch (error) {
            // Email must be unique. Catch error thrwon when duplicated email
            // 11000 error code for mongoose unique filed eroor
            if (error.code == '11000') {
                throw Error("Failed, please try with new information")
            }
            throw error
        }
    }

    /**
    * Delete user
    * @param {String} uid
    * @returns {Promise} Promise<Users>
    */
    async deleteUser(uid) {
        try {
            if (!uid || !helper.isValidLength(uid, 8)) {
                throw new RequiredParameterException(400, 'uid')
            }
            const result = await this.usersRepo.deleteUser(uid);

            if (!result) {
                throw new NotFoundException(404, 'User')
            }

            return result;
        } catch (error) {
            throw error
        }

    }

     /**
    * Delete all users
    */
   async deleteAllUser() {
    try {
        return await this.usersRepo.deleteAllUsers();
    } catch (error) {
        throw error
    }

}
}

module.exports = UsersService;