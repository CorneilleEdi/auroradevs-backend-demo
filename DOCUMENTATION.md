# Documentation of the API (Architecture and Fonctionalities)

>THe following files and folders have to be ignore
>nginx/
>.gitlab-ci.yml
>Dockerfile
>docker-compose.yml

The backend is a simple CRUD API that allows to perform some operation like:

- get the list of user
- get a user information
- save a user into a database
- update user information
- delete a user

The base endpoint is : `/api/users`

### Get the list of user

request type : `GET`
params : no
body: no
status: 200 or 500
response:

```json
[
  {
    "about": "Quia qui voluptatem ut esse rerum incidunt quasi.",
    "_id": "5e422c136867f013dff28d2e",
    "email": "Roxanne37@gmail.com",
    "name": "Aurelie Reinger",
    "uid": "34fb8303-cc25-4e65-8156-d4338ae4753c",
    "createdDate": "2020-02-11T04:22:43.453Z"
  },
  {
    "about": "Aperiam vel perspiciatis dolor deserunt labore maxime sint.",
    "_id": "5e422c156867f013dff28d2f",
    "email": "Melissa_Bode34@gmail.com",
    "name": "Anastasia Kuhn",
    "uid": "096c905a-83bb-4285-8f00-6f252fdb4426",
    "createdDate": "2020-02-11T04:22:45.302Z"
  }
]
```

### Get one user information

request type : `GET`
params : user id (length>8 char)
body: no
status: 200 / 400 / 404 / 500
response:

```json

{
    "about": "Quia qui voluptatem ut esse rerum incidunt quasi.",
    "_id": "5e422c136867f013dff28d2e",
    "email": "Roxanne37@gmail.com",
    "name": "Aurelie Reinger",
    "uid": "34fb8303-cc25-4e65-8156-d4338ae4753c",
    "createdDate": "2020-02-11T04:22:43.453Z"
}
```

### Create a user 

request type : `POST`
params : no
body: 

```json
{
    "about": "Quia qui voluptatem ut esse rerum incidunt quasi.",
    "email": "Roxanne37@gmail.com",
    "name": "Aurelie Reinger"
}
```

status: 201 / 400 / 500
response:

```json

{
    "about": "Quia qui voluptatem ut esse rerum incidunt quasi.",
    "_id": "5e422c136867f013dff28d2e",
    "email": "Roxanne37@gmail.com",
    "name": "Aurelie Reinger",
    "uid": "34fb8303-cc25-4e65-8156-d4338ae4753c",
    "createdDate": "2020-02-11T04:22:43.453Z"
}
```
validation:
- valid email
- about is optional
- name length > 4 char


### Update a user 

request type : `PATCH`
params : user id (length>8 char)
body: 

```json
{
    "about": "Quia qui voluptatem ut esse rerum incidunt quasi.",
    "email": "Roxanne37@gmail.com",
    "name": "Aurelie Reinger"
}
```

status: 202 / 400 / 500
response:

```json

{
    "about": "Quia qui voluptatem ut esse rerum incidunt quasi.",
    "_id": "5e422c136867f013dff28d2e",
    "email": "Roxanne37@gmail.com",
    "name": "Aurelie Reinger",
    "uid": "34fb8303-cc25-4e65-8156-d4338ae4753c",
    "createdDate": "2020-02-11T04:22:43.453Z"
}
```
validation:
- valid is optional
- about is optional
- name optional
- if given, same as user creation

### Delete a user 

request type : `DELETE`
params : user id (length>8 char)
body: no
status: 200 / 400 / 500
response:

```json

{
    "about": "Quia qui voluptatem ut esse rerum incidunt quasi.",
    "_id": "5e422c136867f013dff28d2e",
    "email": "Roxanne37@gmail.com",
    "name": "Aurelie Reinger",
    "uid": "34fb8303-cc25-4e65-8156-d4338ae4753c",
    "createdDate": "2020-02-11T04:22:43.453Z"
}
```
