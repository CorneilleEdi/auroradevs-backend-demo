# Use Node v13.6.0 as the base image.
FROM node:13.6.0

#Set the working directory
WORKDIR /usr/app

# Copy package files
COPY package.json package-lock.json* ./

# Install dependencies
RUN yarn install

# Copy everything in current directory to /server folder
COPY . .


# Run node
CMD ["node", "./src/server.js"]
