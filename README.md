# Example backend app for Aurora Devs 🌞 🌕 🌟 ✨ 💻

## How to use

---

### Clone the repo using git clone command

### install dependencies using npm or yarn

#### using npm

```bash
npm install
```

#### using yarn

```bash
yarn
```

### Config your environement

create a `.env` file at the root

```env
PORT=<your server port>,
DB_URI=<your mongodb connection url>,
DB_USERNAME=<your mongodb username>,
DB_PASSWORD=<your mongodb password>,
```

you can leave the fileds empty to use the default config.


### run the server

#### using npm

```bash
npm start
```

#### using yarn

```bash
yarn start
```
